(* 1. Write a recursive function pow, which takes two integer parameters x and n, and returns x  n
  . Also write a
  function float pow, which does the same thing, but for x being a float. n is still a non-negative integer. *)

  let rec pow x n = match n with 
    |0->1
    |_->x * pow x (n-1);;

  (* for float number *)

    let rec float_pow x n = match n with 
    |0->1.0
    |_->x *. float_pow x (n-1);;

(* 2. Write a function compress to remove consecutive duplicates from a list. 
    # compress ["a";"a";"b";"c";"c";"a";"a";"d";"e";"e";"e"];;
    - : string list = ["a"; "b"; "c"; "a"; "d"; "e"] *)

    let rec compress = function
        | previous_node :: current_node :: next_node -> 
            if previous_node = current_node then 
              compress (current_node :: next_node) 
            else 
              previous_node :: compress (current_node :: next_node)
        | single_node -> 
          single_node;;

(* 3. Write a function remove_if of the type 'a list -> ('a -> bool) -> 'a list, which takes a list and 
    a predicate, and removes all the elements that satisfy the condition expressed in the predicate.
    # remove_if [1;2;3;4;5] (fun x -> x mod 2 = 1);;
    - : int list = [2; 4] *)


   
    let rec remove_if mylist some_fun= match mylist with 
      |[]       -> []
      |hd :: tl ->
        if some_fun hd =true then 
          remove_if tl some_fun
        else
          hd::remove_if tl some_fun;;

(* 4. Some programming languages (like Python) allow us to quickly slice a list based on two integers i and 
  j, to return the sublist from index i (inclusive) and j (not inclusive). We want such a slicing function
  in OCaml as well.
  Write a function slice as follows: given a list and two indices, i and j, extract the slice of the list
  containing the elements from the ith (inclusive) to the jth (not inclusive) positions in the original list.
  # slice ["a";"b";"c";"d";"e";"f";"g";"h"] 2 6;;
  - : string list = ["c"; "d"; "e"; "f"] *)



  let rec slice mylist start_slice end_slice = 
    match mylist with 
      |[]   -> [] 
      |hd::tl->
      
       if start_slice > end_slice then 
        []
        else if start_slice < 1 && end_slice > 0 then 
          hd::slice tl (start_slice-1) (end_slice-1) 
         else
          slice tl (start_slice-1) (end_slice-1) 
      ;;


(* 5. Write a function equivs of the type ('a -> 'a -> bool) -> 'a list -> 'a list list, which 
      partitions a list into equivalence classes according to the equivalence function.
      # equivs (=) [1;2;3;4];;
      - : int list list = [[1];[2];[3];[4]]
         equivs (fun x y -> (=) (x mod 2) (y mod 2)) [1; 2; 3; 4; 5; 6; 7; 8];;
      - : int list list = [[1; 3; 5; 7]; [2; 4; 6; 8]] *)

      

        let equivs_helper f lst =
          let rec find myfunction the_head find_acc = 
            match find_acc with
            |[]-> [[the_head]]
            | hd::tl -> 
              match hd with 
              h::_-> 
                if f h the_head then 
                  (the_head::hd)::tl 
                else hd::(find f the_head tl)
              |_->[]
          in
          let rec convert thefuntion list accumulator = 
            match list with
            |[] -> accumulator
            | ht::tl -> convert f tl (find f ht accumulator) 
          in 
          
          match lst with 
          []->[]
          | hd::tl -> convert f tl [[hd]]
 
      ;;
      
        let revese mylist =
            let rec aux accumulator = function
              | [] -> accumulator
              | hd::tl -> aux (hd::accumulator) tl in
            aux [] mylist;;

       let equivs f list = 
        match (equivs_helper f list) with
      |[]       ->[]
      | hd::tl -> (revese hd )::
          let rec second_reverse tl=
            match tl with 
            |[] ->[]
            |hd::tl -> (revese hd) :: second_reverse(tl) 
          in second_reverse tl;;


    

(* 6. Goldbach’s conjecture states that every positive even number greater than 2 is the sum of two prime 
  numbers. E.g., 18 = 5 + 13, or 42 = 19 + 23. It is one of the most famous conjectures in number theory.
  It is unproven, but verified for all integers up to 4 × 1018. Write a function goldbachpair : int ->
  int * int to find two prime numbers that sum up to a given even integer. The returned pair must have
  a non-decreasing order. *)



  let check_prime primenumber = 
    let rec check_prime i prime_number=
          if i=prime_number then true
        else if prime_number <= 1 then false
        else if (prime_number mod i) = 0 then 
          false
        else  check_prime (i+1) prime_number
    in check_prime 2 primenumber;
    ;;


  let goldbachpair int_to_convert= 

    let rec inside_fun first_num int_to_convert= 
      if check_prime first_num then begin

        let second_num=int_to_convert-first_num in 
            
        if check_prime second_num then first_num,second_num
        else inside_fun (first_num+1) int_to_convert
      end else
        (* firstnum is not prime *)
        inside_fun (first_num+1) int_to_convert
        in 
      inside_fun 1 int_to_convert; 
    ;;

(* 7. Write a function called equiv on, which takes three inputs: two functions f and g, and a list lst. It 
  returns true if and only if the functions f and g have identical behavior on every element of lst.
  # let f i = i * i;;
  val f : int -> int = <fun>
  # let g i = 3 * i;;
  val g : int -> int = <fun>
  # equiv_on f g [3];;
  - : bool = true
  # equiv_on f g [1;2;3];;
  - : bool = false *)

  let rec equiv_on f g lst = match lst with 
        | []  ->true
        |hd::tl-> 
          if (=) (f hd) (g hd) then 
            true && equiv_on f g tl
          else
            false;;

(* 8. Write a functions called pairwisefilter with two parameters: (i) a function cmp that compares two 
  elements of a specific T and returns one of them, and (ii) a list lst of elements of that same type T. It
  returns a list that applies cmp while taking two items at a time from lst. If lst has odd size, the last
  element is returned “as is”.
  # pairwisefilter min [14; 11; 20; 25; 10; 11];;
  - : int list = [11; 20; 10]
  # (* assuming that shorter : string * string -> string = <fun> already exists *)
  # pairwisefilter shorter ["and"; "this"; "makes"; "shorter"; "strings"; "always"; "win"];;
  - : string list = ["and"; "makes"; "always"; "win"] *)

    let rec pairwisefilter cmd lst = match lst with 
        |[]       ->[]
        |hd::x::tl->
            cmd hd x ::pairwisefilter cmd tl 
        |hd        -> hd;;

(* 9. Write the polynomial function, which takes a list of tuples and returns the polynomial function corresponding 
  to that list. Each tuple in the input list consists of (i) the coefficient, and (ii) the exponent.
  # (* below is the polynomial function f(x) = 3x^3 - 2x + 5 *)
  # let f = polynomial [3, 3; -2, 1; 5, 0];;
  val f : int -> int = <fun>
  # f 2;;
  - : int = 25 *)

          let rec polynomial list_of_tuples = fun x-> 
            let rec sumation list x = 
                match list with 
                |[]       -> 0 
                | (first,second)::tl ->  first*(pow x second)+ sumation tl x
            in sumation list_of_tuples x;
          ;;
         
(* 10. The power set of a set S is the set of all subsets of S (including the empty set and the entire set). )
  Write a function powerset of the type 'a list -> 'a list list, which treats lists as unordered sets,
  and returns the powerset of its input list. You may assume that the input list has no duplicates.
  # powerset [3; 4; 10];;
   : int list list = [[]; [3]; [4]; [10]; [3; 4]; [3; 10]; [4; 10]; [3; 4; 10]]; *)
  
  
    let rec powerset mylist = match mylist with
    | [] -> [[]]
    | first :: second -> 
      let insidefun = powerset second in
      insidefun @ List.map (fun funvar -> first :: funvar) insidefun;;
             