(* 1. Let us define a language for expressions in Boolean logic: (10)
  type bool_expr =
  | Lit of string
  | Not of bool_expr
  | And of bool_expr * bool_expr
  | Or of bool_expr * bool_expr
  using which we can write expressions in prefix notation. E.g., (a∧b)∨(¬a) is Or(And(Lit("a"), Lit("b")),
  Not(Lit("a"))). Your task is to write a function truth table, which takes as input a logical expression
  in two literals and returns its truth table as a list of triples, each a tuple of the form:
  (truth-value-of-first-literal, truth-value-of-second-literal, truth-value-of-expression)
  For example,
  # (* the outermost parentheses are needed for OCaml to parse the third argument
  correctly as a bool_expr *)
  # truth_table "a" "b" (And(Lit("a"), Lit("b")));;
  - : (bool * bool * bool) list = [(true, true, true); (true, false, false);
  (false, true, false); (false, false, false)] *)

  type bool_expr =
    | Lit of string
    | Not of bool_expr
    | And of bool_expr * bool_expr
    | Or of bool_expr * bool_expr;;

    let rec convert a representation_of_a b representation_of_b = function
    | Not expression -> 
      not(convert a representation_of_a b representation_of_b expression)
    | And(first, second) -> 
      convert a representation_of_a b representation_of_b first 
      && 
      convert a representation_of_a b representation_of_b second
    | Or(first, second) -> 
      convert a representation_of_a b representation_of_b first 
      || 
      convert a representation_of_a b representation_of_b second
    | Lit mystring -> 
      if mystring = a then 
        representation_of_a
      else representation_of_b;;

  let truth_table a b expr =
    [(true, true,  convert a true b true expr); (true, false,convert a true  b false expr);
     (false,true,  convert a false b true expr); (false,false,convert a false b false expr) ];;

