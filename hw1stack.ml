
(* 2. In this question you will use higher-order functions to implement an interpreter for a simple stack-based 
  evaluation language. This language has a fixed set of commands:
  • start → Initializes an empty stack. This is always the first command in a program and never
  appears again.
  • (push n) → Pushes the specified integer n on to the top of the stack. This command is always
  parenthesized.
  • pop → Removes the top element of the stack.
  • add → Pops the top two elements of the stack, computes their sum, and pushes the result back on
  to the stack.
  • mult → Pops the top two elements of the stack, computes their product, and pushes the result back
  on to the stack.
  • clone → Pushes a duplicate copy of the top element on to the stack.
  • kpop → Pops the top element k of the stack, and if k is a positive number, pops k more elements
  (or the stack becomes empty, whichever happens sooner).
  • halt → Terminates the stack evaluation program. This is always the last command.
  Your task is to define the stack data structure in OCaml (10 points), and then implement the above
  commands (20 points). Your stack must be implemented as a list. A complete running example would
  look something like the following:
  # start (push 2) (push 3) (push 4) mult add halt;;
  - : list int = [14]
  You may assume that only valid commands will be provided. As such, you do not have to worry about
  exception handling. *)



  type 'a stack = {data : 'a list};;

  let start f = f {data=[]};;

    let push x acc secondf= 
      secondf  {data = (x::acc.data) };;




let pop acc secondf= secondf(
match acc.data with 
|[]     -> {data =[]}
|hd::tl -> {data= tl }
);;


let add acc secondf= secondf(
match acc.data with 
|[]     -> {data =[]}
|hd::x::tl -> {data =(hd+x)::tl} 
|_::_->{data =[]}
);;

let mult acc secondf= secondf(
match acc.data with 
|[]     -> {data =[]}
|hd::x::tl -> {data =(hd*x)::tl}
|_::_->{data =[]}
);;

let clone acc secondf= secondf(
match acc.data with 
|[]->{data =[]}
|hd::tl->{data =hd::hd::tl} 


);;


let kpop acc secondf= secondf(
match acc.data with 
|[]->{data =[]}
|hd::tl-> 
if hd > 0 then
    let rec pop list k counter=
      match list with 
      |[]-> {data = []} 
      |hd::tl-> 
        if k= (counter+1) then  {data = tl} 
        else  
        pop tl k (counter+1)
      
    in pop tl hd 0
    
else 
  {data = tl} 

);;

let halt a= a;;





